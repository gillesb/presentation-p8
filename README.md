# Presentation P8



Ceci est un modèle (en quatre versions) de présentation utilisé en licence et master d'informatique à l'université Paris 8, suivant les consignes de ces diplômes.

Soutenance.tex est la version correspondant à une soutenance de projet de master

Realisation.tex est la version correspondant à une soutenance de projet de licence

Problem.tex est une version adaptée à la présentation d'une problématique, correspondant à un milieu de projet (plutôt master)

Domaine.tex est la version adaptée à la présentation d'un domaine de recherche hors projet
